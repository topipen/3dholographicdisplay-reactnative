### Introduction ###

3D holographic display control mobile application. Implemented with React Native.

Web app & server: https://bitbucket.org/topipen/3dholographicdisplay-web

### Setting up ###

Clone & run

```
npm install
```