/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, {Component} from 'react';
import * as firebase from 'firebase';
import {
  AppRegistry,
  Alert,
  ListView,
  Text,
  View,
  Button,
} from 'react-native';

var videonumber = 1;

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDlFoptdmu-e-rOBuTAXJYEqOOgXyygvN0",
  authDomain: "dholographicdisplay.firebaseapp.com",
  databaseURL: "https://dholographicdisplay.firebaseio.com",
  projectId: "dholographicdisplay",
  storageBucket: "dholographicdisplay.appspot.com",
  messagingSenderId: "567753264902"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

const onButtonPress = () => {
  if (videonumber == 1) {
    videonumber = 2
    firebase.database().ref('currentFile').set({
    filename: "test2.mp4",
  });
  } else {
    videonumber = 1
    firebase.database().ref('currentFile').set({
    filename: "test.mp4",
  });
  }
};

class ProtoApp extends Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2})
    this.state = {
      dataSource: ds.cloneWithRows([
        'Video 1', 'Video 2'
      ])
    };
  }
  render() {
    return (
      <View style={{flex: 1, paddingTop: 22}}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <Text>{rowData}</Text>}
        />
        <Button
          onPress={onButtonPress}
          title="Change video"
          color="#841584"
          accessibilityLabel="Change the playing video"
        />
      </View>
    );
  }
}

AppRegistry.registerComponent('ProtoApp', () => ProtoApp);
